class V1::ContentsController < ApplicationController
    before_action :set_content, only: [:show, :update, :destroy]

    def index 
        @contents = current_user.contents.paginate(page: params[:page], per_page: 20)
        json_response(@contents, :ok)
    end

    def create 
        @content = Content.create!(content_params.merge(user_id: current_user.id))
        json_response(@content, :created)
    end

    def show 
        json_response(@content)
    end

    def update 
        @content.update(content_params)
        head :no_content
    end

    def destroy 
        @content.destroy
        head :no_content
    end

    private 
        def content_params
            params.permit(
                :name, :description, :sequence, :publish
            )
        end

        def set_content
            @content = Content.find_by(user_id: current_user.id, id: params[:id]) 
        end
end
