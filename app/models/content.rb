class Content < ApplicationRecord
  belongs_to :user
  has_many :sub_content, dependent: :destroy

  validates_presence_of :name, :description
end