class SubContent < ApplicationRecord
  belongs_to :content

  validates_presence_of :name, :description
end
