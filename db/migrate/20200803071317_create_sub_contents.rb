class CreateSubContents < ActiveRecord::Migration[6.0]
  def change
    create_table :sub_contents do |t|
      t.string :tag
      t.string :name
      t.text :description, :limit => 4294967295
      t.boolean :publish, :default => true
      t.string :sublevel_id
      t.integer :sublevel_sequence
      t.integer :sequence
      t.references :content, null: false, foreign_key: true

      t.timestamps
    end
  end
end
