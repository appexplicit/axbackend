class AddFirstNameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :mobile, :string
    add_column :users, :phone, :string
    add_column :users, :facebook_id, :text, :limit => 16777215
    add_column :users, :google_id, :text, :limit => 16777215
  end
end