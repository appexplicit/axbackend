class CreateContents < ActiveRecord::Migration[6.0]
  def change
    create_table :contents do |t|
      t.string :tag
      t.string :name
      t.text :description, :limit => 4294967295
      t.integer :sequence
      t.boolean :publish, :default => true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
