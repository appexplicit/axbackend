Rails.application.routes.draw do

  scope module: :v1, constaints: ApiVersion.new("v1", true) do
    resources :todos do
      resources :items
    end
  end

  scope module: :v1, path: "user" do
    resources :contents do
      resources :sub_contents
    end
  end
  
  post "auth/login", to: "authentication#authenticate"
  post "auth/password-recovery", to: "users#password_recovery"
  post "auth/reset-password", to: "users#reset_password"
  post "/signup", to: "users#create"
end
