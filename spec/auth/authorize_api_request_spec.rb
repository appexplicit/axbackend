require 'rails_helper'

RSpec.describe AuthorizeApiRequest do
    let(:user) { create(:user) }
    let(:header) {  { 'Authorization' => token_generator(user.id) } }

    subject(:invalid_request_obj) { described_class.new({}) }

    subject(:request_obj) { described_class.new(header) }

    define "#call" do
        context "when request is valid" do
            it "returns user object" do
                result = request_obj.call
                expect(result[:user]).to eq(user)
            end
        end

        context "when invalid token" do
            context "when missing token" do
                it "raise a missing token error" do
                    expect { invalid_request_obj.call }.to raise_error(ExceptionHandler::MissingToken, /Missing Token/)
                end
            end
            
            context "when invalid token" do
                subject(:invalid_request_token) do
                    described_class.new({ 'Authorization' => token_generator(5) })
                end

                it "raise an invalid token error" do
                    expect { invalid_request_token.call }.to raise_error(ExceptionHandler::InvalidToken, /Invalid Token/)
                end
            end

            context "when token is expired" do
                let(:header) { { 'Authorization' => expired_token_generator(user.id) } }
                subject(:request_obj) { described_class.new(header) }

                it 'raises ExceptionHandler::ExpiredSignature error' do
                    expect { request_obj.call }.to raise_error(ExceptionHandler::InvalidToken, /Signature has expired/)
                end
            end

            context "fake token" do
                let(:header) { { 'Authorization' => 'foobar' } }
                subject(:invalid_request_obj) { described_class.new(header) }

                it 'handles JWT::DecodeError' do
                    expect { invalid_request_obj.call }.to raise_error(ExceptionHandler::InvalidToken,  /Not enough or too many segments/)
                end
            end
        end
    end

end