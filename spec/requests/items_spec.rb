require 'rails_helper'

RSpec.describe 'Items API' do
    let(:user) { create(:user) }
    let!(:todo) { create(:todo) }
    let!(:items) { create_list(:item, 20, todo_id: todo.id) }
    let(:todo_id) { todo.id }
    let(:id) { items.first.id }
    let(:headers) { valid_headers }

    describe "GET /todos/:todo_id/items" do
        before { get "/todos/#{todo_id}/items", params: {}.to_json, headers: headers }

        context "when todo exists" do
            it "returns status code 200" do
                expect(response).to have_http_status(200)
            end

            it "returns all todo items" do
                expect(json.size).to eq(20)
            end
        end

        context "when todo does not exists" do
            let(:todo_id) { 0 }
            it "return status code 404" do
                expect(response).to have_http_status(404)
            end

            it "return a not found message" do
                expect(response.body).to match(/Couldn't find/)
            end
        end
    end

    describe "POST /todos/:todo_id/items" do
        let(:valid_attributes) { {name: "Visit Narnia", done: false } }
        
        context "when request attribute are valid" do
            before { post "/todos/#{todo_id}/items", params: valid_attributes.to_json, headers: headers  }

            it "return status code 201" do
                expect(response).to have_http_status(201)
            end
        end

        context "when an invalid request" do
            before { post "/todos/#{todo_id}/items", params: {}.to_json, headers: headers }

            it "return status code 422" do
                expect(response).to have_http_status(422)
            end

            it "returns a failure message" do
                expect(response.body).to match(/Validation failed/)
            end
        end
    end

    describe "PUT /todos/:todo_id/items/:id" do
        let(:valid_attributes) { {name: 'Mozart' } }

        before { put "/todos/#{todo_id}/items/#{id}", params: valid_attributes.to_json, headers: headers }

        context "when item exists" do
            it "returns status code 204" do
                expect(response).to have_http_status(204)
            end

            it "updates the item" do
                updated_item = Item.find(id)
                expect(updated_item.name).to match(/Mozart/)
            end
        end

        context "when item does not exists" do
            let(:id) { 0 }

            it "returns status code 404" do
                expect(response).to have_http_status(404)
            end

            it "returns a not found message" do
                expect(response.body).to match(/Couldn't find/)
            end
        end
    end

    describe "DESTROY /todos/:id" do
        before { delete "/todos/#{todo_id}/items/#{id}", params: {}.to_json, headers: headers }

        it "should return status code 204" do
            expect(response).to have_http_status(204)
        end

        context "when record does not exists at all" do
            before { get "/todos/#{todo_id}/items/#{id}", params: {}, headers: headers }
    
            it "should return 404" do
                expect(response).to have_http_status(404)
            end
        end
        
    end
end