require 'rails_helper'

RSpec.describe "Contents API", type: :request do
    let!(:user) { create(:user) }
    let!(:content) { create_list(:content, 1, user_id: user.id) }
    let!(:sub_content) { create_list(:sub_content, 10, content_id: content.first.id) }
    let!(:content_id) { content.first.id }
    let!(:headers) { valid_headers }

    describe "GET /user/contents" do
        context "when record is valid" do
            before { get "/user/contents", params: { user_id: user.id }, headers: headers }

            it "returns all contents per user" do
                expect(json).not_to be_empty
                expect(json.size).to eq(1)
            end
    
            it "should return status code 200" do
                expect(response).to have_http_status(200)
            end
        end

        context "when record is invalid" do
            before { get "/user/contents", params: { user_id: user.id }, headers: invalid_headers }

            it "Should return missing token status" do
                expect(response.body).to match(/Missing Token/)
            end
        end
    end

    describe "POST /user/contents" do
        let(:form_data) { { name: "Test", description: "Desc", publish: true, sequence: 0 } }

        before { post "/user/contents", params: form_data.to_json, headers: valid_headers }

        it "should have status 201" do
            expect(response).to have_http_status(201)
        end
    end

    describe "PUT /user/contents/:id" do
        let(:form_data) { { name: "Another Update", description: "Updated", publish: false, sequence: 1} }

        before { put "/user/contents/#{content_id}", params: form_data.to_json, headers: headers }

        it "should have status 204" do
            expect(response).to have_http_status(204)
        end
    end

    describe "DESTROY /user/contents/:id" do
        before { delete "/user/contents/#{content_id}", params: {}, headers: headers }

        it "should have status 204" do
            expect(response).to have_http_status(204)
        end
    end

end