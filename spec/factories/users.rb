FactoryBot.define do
    factory :user do
        name { Faker::Name.name }
        email { 'foobar@gmail.com' }
        password { 'foobar' }
    end
end