FactoryBot::define do
    factory :content do
        name { Faker::Name.name }
        description { Faker::Job.title }
        publish { true }
    end
end