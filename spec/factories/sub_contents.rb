FactoryBot::define do
    factory :sub_content do
        name { Faker::Games::StreetFighter.character }
        description { Faker::Games::StreetFighter.quote }
        publish { true }
        sublevel_sequence { 1 }
    end
end