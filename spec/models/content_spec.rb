require 'rails_helper'

RSpec.describe Content, type: :model do
 it { should have_many(:sub_content) }
 it { should validate_presence_of(:name) }
 it { should validate_presence_of(:description) }
end
