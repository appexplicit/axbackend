require 'rails_helper'

RSpec.describe SubContent, type: :model do
  it { should belong_to(:content) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }
end
